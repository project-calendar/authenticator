(() => {
    'use strict'

    const displayNone = "display: none;";
    const displayBlock = "display: block;"

    const changePasswordInputVisibility = (hide, container) => {
            Array.from(container.getElementsByClassName("show")).forEach(button => {
                button.style = hide ? displayBlock : displayNone;
            });
            Array.from(container.getElementsByClassName("hide")).forEach(button => {
                button.style = hide ? displayNone : displayBlock;
            });
            Array.from(container.getElementsByTagName("input")).forEach(input => {
                input.type = hide ? "password" : "text";
            });
    };
  
    // Fetch all the forms we want to apply custom Bootstrap validation styles to
    const forms = document.querySelectorAll('.needs-validation')
    // Loop over them and prevent submission
    Array.from(forms).forEach(form => {
      form.addEventListener('submit', event => {
        if (!form.checkValidity()) {
          event.preventDefault()
          event.stopPropagation()
        }
  
        form.classList.add('was-validated')
      }, false)
    });
    const passwordFieldsContainer = document.querySelectorAll('.change-password-type');
    Array.from(passwordFieldsContainer).forEach(container => {
        Array.from(container.getElementsByClassName("vis-button")).forEach(button => {
            button.addEventListener('click', event => {
                const isHide = button.classList.contains("hide");
                changePasswordInputVisibility(isHide, container);
            });
        });
    });

  })()