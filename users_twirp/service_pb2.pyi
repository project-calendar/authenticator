from google.protobuf.internal import containers as _containers
from google.protobuf import descriptor as _descriptor
from google.protobuf import message as _message
from typing import ClassVar as _ClassVar, Iterable as _Iterable, Mapping as _Mapping, Optional as _Optional, Union as _Union

DESCRIPTOR: _descriptor.FileDescriptor

class OauthProvider(_message.Message):
    __slots__ = ["name"]
    NAME_FIELD_NUMBER: _ClassVar[int]
    name: str
    def __init__(self, name: _Optional[str] = ...) -> None: ...

class UserUniqueIdentifiers(_message.Message):
    __slots__ = ["id", "email"]
    ID_FIELD_NUMBER: _ClassVar[int]
    EMAIL_FIELD_NUMBER: _ClassVar[int]
    id: str
    email: str
    def __init__(self, id: _Optional[str] = ..., email: _Optional[str] = ...) -> None: ...

class UserWithPassword(_message.Message):
    __slots__ = ["password", "id", "email"]
    PASSWORD_FIELD_NUMBER: _ClassVar[int]
    ID_FIELD_NUMBER: _ClassVar[int]
    EMAIL_FIELD_NUMBER: _ClassVar[int]
    password: str
    id: str
    email: str
    def __init__(self, password: _Optional[str] = ..., id: _Optional[str] = ..., email: _Optional[str] = ...) -> None: ...

class CreateUserData(_message.Message):
    __slots__ = ["email", "username", "password", "profile_picture_url", "providers"]
    EMAIL_FIELD_NUMBER: _ClassVar[int]
    USERNAME_FIELD_NUMBER: _ClassVar[int]
    PASSWORD_FIELD_NUMBER: _ClassVar[int]
    PROFILE_PICTURE_URL_FIELD_NUMBER: _ClassVar[int]
    PROVIDERS_FIELD_NUMBER: _ClassVar[int]
    email: str
    username: str
    password: str
    profile_picture_url: str
    providers: _containers.RepeatedCompositeFieldContainer[OauthProvider]
    def __init__(self, email: _Optional[str] = ..., username: _Optional[str] = ..., password: _Optional[str] = ..., profile_picture_url: _Optional[str] = ..., providers: _Optional[_Iterable[_Union[OauthProvider, _Mapping]]] = ...) -> None: ...

class Organization(_message.Message):
    __slots__ = ["id", "name", "type"]
    ID_FIELD_NUMBER: _ClassVar[int]
    NAME_FIELD_NUMBER: _ClassVar[int]
    TYPE_FIELD_NUMBER: _ClassVar[int]
    id: str
    name: str
    type: str
    def __init__(self, id: _Optional[str] = ..., name: _Optional[str] = ..., type: _Optional[str] = ...) -> None: ...

class User(_message.Message):
    __slots__ = ["id", "email", "username", "profile_picture_url", "grants", "organizations"]
    ID_FIELD_NUMBER: _ClassVar[int]
    EMAIL_FIELD_NUMBER: _ClassVar[int]
    USERNAME_FIELD_NUMBER: _ClassVar[int]
    PROFILE_PICTURE_URL_FIELD_NUMBER: _ClassVar[int]
    GRANTS_FIELD_NUMBER: _ClassVar[int]
    ORGANIZATIONS_FIELD_NUMBER: _ClassVar[int]
    id: str
    email: str
    username: str
    profile_picture_url: str
    grants: _containers.RepeatedScalarFieldContainer[str]
    organizations: _containers.RepeatedCompositeFieldContainer[Organization]
    def __init__(self, id: _Optional[str] = ..., email: _Optional[str] = ..., username: _Optional[str] = ..., profile_picture_url: _Optional[str] = ..., grants: _Optional[_Iterable[str]] = ..., organizations: _Optional[_Iterable[_Union[Organization, _Mapping]]] = ...) -> None: ...
