import logging
from enum import Enum

from fastapi import FastAPI, Request
from pydantic import BaseModel
from twirp.errors import Errors

from authenticator.config import Settings
from users_twirp.service_twirp import ProjectCalendarUsersClient


class TypedApp(FastAPI):
    twirp_client: ProjectCalendarUsersClient
    logger: logging.Logger
    config: Settings


class TypedRequest(Request):
    app: TypedApp


class LoginError(BaseModel):
    code: str
    errors: set[Errors]
    message: str


class LoginErrors(Enum):
    BAD_CREDENTIALS = LoginError(
        code="bad-credentials",
        errors={Errors.NotFound, Errors.PermissionDenied},
        message="Wrong email or password.",
    )
    IDP_ONLY = LoginError(
        code="idp-only",
        errors={Errors.ResourceExhausted},
        message="This email is assigned to an account that can be logged in using external identity provider.",
    )
    UNKNOWN = LoginError(code="unknown", errors=set(Errors), message="Unknown error.")

    @classmethod
    def get_by_error_code(cls, code: Errors) -> "LoginErrors":
        for error in cls:
            if code in error.value.errors:
                return error

    @classmethod
    def get_by_code(cls, code: str | None):
        for error in cls:
            if error.code == code:
                return error

    @property
    def message(self) -> str:
        return self.value.message

    @property
    def code(self) -> str:
        return self.value.code


ERROR_CODES = Enum("ERROR_CODES", {error.name: error.code for error in LoginErrors})
