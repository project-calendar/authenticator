from authlib.integrations.starlette_client import OAuth, OAuthError

from authenticator.providers import Provider

oauth = OAuth()
oauth.register(
    name=Provider.GOOGLE.value.name,
    server_metadata_url="https://accounts.google.com/.well-known/openid-configuration",
    client_id=Provider.GOOGLE.value.consumer_key,
    client_secret=Provider.GOOGLE.value.consumer_secret,
    client_kwargs={"scope": " ".join(Provider.GOOGLE.value.scope)},
)

oauth.register(
    name=Provider.MICROSOFT.value.name,
    server_metadata_url="https://login.microsoftonline.com/consumers/v2.0/.well-known/openid-configuration",
    client_id=Provider.MICROSOFT.value.consumer_key,
    client_secret=Provider.MICROSOFT.value.consumer_secret,
    client_kwargs={
        "scope": " ".join(Provider.MICROSOFT.value.scope),
        "nonce": 89889,
    },
)

oauth.register(
    name=Provider.INHOUSE.value.name,
    server_metadata_url="http://localhost:8000/.well-known/openid-configuration",
    client_id=Provider.INHOUSE.value.consumer_key,
    client_secret=Provider.INHOUSE.value.consumer_secret,
    client_kwargs={"scope": " ".join(Provider.MICROSOFT.value.scope)},
)
