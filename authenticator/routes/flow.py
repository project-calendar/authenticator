from functools import partial
from typing import TypeAlias

from authlib.jose import jwt
from fastapi import APIRouter, Cookie, Form, Query, status
from fastapi.requests import Request
from fastapi.responses import RedirectResponse
from pydantic import AnyHttpUrl, EmailStr
from twirp.context import Context
from twirp.errors import Errors
from twirp.exceptions import TwirpServerException

from authenticator.oauth_client import OAuthError, oauth
from authenticator.providers import NamedProviders, Provider
from authenticator.utils import LoginErrors, TypedRequest
from users_twirp import service_pb2

router = APIRouter(tags=["flow"])
RedirectUriType: TypeAlias = AnyHttpUrl | None
RedirectUri = Query(None, description="Redirect URI.", alias="redirect_uri")


@router.post("/login")
async def login(
    request: TypedRequest,
    email: EmailStr = Form(...),
    password: str = Form(...),
    redirect_uri: RedirectUriType = RedirectUri,
):
    redirect_uri = redirect_uri or request.url_for("homepage")
    response = partial(RedirectResponse, status_code=status.HTTP_303_SEE_OTHER)
    user = None
    try:
        user: service_pb2.User = request.app.twirp_client.getAndVerifyUser(
            ctx=Context(),
            request=service_pb2.UserWithPassword(email=email, password=password),
        )
        token = jwt.encode(
            {"alg": "HS256"},
            {
                "iss": "http://localhost:8000",
                "sub": user.email,
                "username": user.username,
                "profile_picture_url": user.profile_picture_url,
            },
            "test",
        )
    except TwirpServerException as exc:
        request.app.logger.debug("Failed while verifying user %s.", email)
        redirect_uri += f"?error={LoginErrors.get_by_error_code(exc.code).code}"
    response = response(redirect_uri)
    if user:
        response.set_cookie("ProjectCalendarToken", token.decode("utf-8"))
    return response


@router.get("/idp-login", response_class=RedirectResponse)
async def idp_login(
    request: TypedRequest,
    provider: NamedProviders
    | None = Query(
        Provider.GOOGLE.value.name,
        description="Identity Provider that will be used to login.",
    ),
    _redirect_uri: RedirectUriType = RedirectUri,
):
    match provider:
        case NamedProviders.GOOGLE:
            oauth_provider = oauth.google
        case NamedProviders.MICROSOFT:
            oauth_provider = oauth.microsoft

    redirect_uri = request.url_for("set_cookie")
    response: RedirectResponse = await oauth_provider.authorize_redirect(
        request, redirect_uri
    )
    response.set_cookie(
        "login_redirect_uri", _redirect_uri or request.url_for("homepage")
    )
    response.set_cookie("login_provider", provider.value)
    return response


@router.get("/set_cookie")
async def set_cookie(
    request: TypedRequest,
    login_redirect_uri=Cookie(...),
    login_provider: NamedProviders = Cookie(...),
):
    match login_provider:
        case NamedProviders.GOOGLE:
            oauth_provider = oauth.google
        case NamedProviders.MICROSOFT:
            oauth_provider = oauth.microsoft

    try:
        token = await oauth_provider.authorize_access_token(request)
    except OAuthError as exc:
        request.app.logger.exception(
            "Failed to get access token from provider %s because of %s",
            oauth_provider,
            exc,
        )
        return {"error": f"Unexpected error."}
    provided_user = token.get("userinfo")
    try:
        user: service_pb2.User = request.app.twirp_client.getUser(
            ctx=Context(),
            request=service_pb2.UserUniqueIdentifiers(
                email=provided_user["email"], id=None
            ),
        )
    except TwirpServerException:
        user = request.app.twirp_client.createUser(
            ctx=Context(),
            request=service_pb2.CreateUserData(
                email=provided_user["email"],
                username=provided_user["preferred_username"]
                if "preferred_username" in provided_user
                else provided_user["name"],
                password=None,
                providers=[service_pb2.OauthProvider(name=login_provider.value)]
                if login_provider
                else None,
                profile_picture_url=provided_user.get("picture"),
            ),
        )
    response = RedirectResponse(url=login_redirect_uri)
    if user:
        token = jwt.encode(
            {"alg": "HS256"},
            {
                "iss": "http://localhost:8000",
                "sub": user.email,
                "username": user.username,
                "profile_picture_url": user.profile_picture_url,
            },
            "test",
        )
        response.set_cookie("ProjectCalendarToken", token.decode("utf-8"))

    response.set_cookie("login_provider")
    response.set_cookie("login_redirect_uri")
    return response


@router.get("/logout")
async def logout(request: Request):
    response = RedirectResponse(request.url_for("homepage"))
    response.set_cookie("ProjectCalendarToken")
    response.set_cookie("login_redirect_uri")
    return response
