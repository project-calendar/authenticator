from functools import partial

from authlib.jose import jwt
from authlib.jose.errors import DecodeError
from fastapi import APIRouter, Form, Query, status
from fastapi.requests import Request
from fastapi.responses import HTMLResponse
from fastapi.templating import Jinja2Templates
from pydantic import AnyHttpUrl, EmailStr
from starlette.datastructures import URL
from twirp.context import Context
from twirp.errors import Errors as TwirpErrors
from twirp.exceptions import TwirpServerException

from authenticator.config import config
from authenticator.utils import ERROR_CODES, LoginErrors, TypedRequest
from users_twirp import service_pb2

router = APIRouter(default_response_class=HTMLResponse, tags=["UI"])
templates = Jinja2Templates(directory="templates")
templates.env.globals["URL"] = URL

REDIRECT_URI_QUERY = Query(
    config.DEFAULT_REDIRECT_URI,
    description="An URL to be redirected to after succesfull login/singup.",
)


@router.get("/")
async def homepage(
    request: Request,
    redirect_uri: AnyHttpUrl = REDIRECT_URI_QUERY,
    error: ERROR_CODES | None = Query(default=None),
):
    """Homepage. Contains login/logout UI depending on the state."""
    token = request.cookies.get(config.TOKEN_COOKIE_NAME)
    response = partial(templates.TemplateResponse, "index.jinja2")
    ctx = {"request": request, "data": {}}
    clear_cookies = False
    try:
        user = jwt.decode(token, "test") if token else None
        ctx["data"]["user"] = user
    except DecodeError:
        clear_cookies = True

    if error and (err := LoginErrors.get_by_code(error.value)):
        ctx["error"] = err.message

    response = response(ctx)
    if clear_cookies:
        response.delete_cookie(config.TOKEN_COOKIE_NAME)
    return response


@router.get("/not-found")
async def not_found():
    raise NotImplementedError()


@router.get("/not-authorized")
def not_authorized(request: TypedRequest):
    response = templates.TemplateResponse(
        "not-authorized.jinja2",
        context={"request": request},
        status_code=status.HTTP_401_UNAUTHORIZED,
    )
    response.delete_cookie(config.TOKEN_COOKIE_NAME)
    return response


@router.get("/signup")
async def signup(
    request: Request,
    redirect_uri: AnyHttpUrl = REDIRECT_URI_QUERY,
):
    """Provides signup page."""
    token = request.cookies.get("ProjectCalendarToken")
    user = jwt.decode(token, "test") if token else None
    return templates.TemplateResponse(
        "index.jinja2", {"request": request, "ctx": "signup", "user": user}
    )


@router.post("/signup", status_code=status.HTTP_201_CREATED)
async def create_user(
    request: TypedRequest,
    email: EmailStr = Form(),
    username: str = Form(),
    password: str = Form(),
    repeated_password: str = Form(),
):
    error = None
    response = partial(templates.TemplateResponse, "index.jinja2")
    context = {"request": request, "ctx": "signup"}
    status_code = status.HTTP_201_CREATED
    if repeated_password != password:
        context["error"] = "Passwords are not the same."
        return response(
            context=context, status_code=status.HTTP_422_UNPROCESSABLE_ENTITY
        )
    try:
        user: service_pb2.User = request.app.twirp_client.createUser(
            ctx=Context(),
            request=service_pb2.CreateUserData(
                email=email, password=password, providers=[], username=username
            ),
        )
        context["ctx"] = "signup-success"
        context["data"] = {"email": user.email}
    except TwirpServerException as exc:
        match exc.code:
            case TwirpErrors.InvalidArgument:
                error = exc.message.split(" ", maxsplit=1)[-1]
                status_code = status.HTTP_400_BAD_REQUEST
            case TwirpErrors.AlreadyExists:
                error = exc.message
                status_code = status.HTTP_405_METHOD_NOT_ALLOWED
            case _:
                error = "Unknown exception."
                request.app.logger.critical(
                    "Failed to send Twirp request with code %s", exc.code
                )
                status_code = status.HTTP_503_SERVICE_UNAVAILABLE
        context["error"] = error
    response = response(context=context, status_code=status_code)
    response.set_cookie(config.TOKEN_COOKIE_NAME)

    return response
