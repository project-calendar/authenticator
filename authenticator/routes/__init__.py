from fastapi import FastAPI

from authenticator.routes.flow import router as flow_router
from authenticator.routes.ui import router as ui_router


def bootstrap_routers(app: FastAPI):
    for router in (ui_router, flow_router):
        app.include_router(router)
