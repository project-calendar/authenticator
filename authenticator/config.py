import logging

from pydantic import AnyHttpUrl, BaseSettings


class GoogleSecrets(BaseSettings):
    CLIENT_ID: str
    CLIENT_SECRET: str

    class Config:
        env_file = ".env"
        env_file_encoding = "utf-8"
        case_sensitive = False
        env_prefix = "GOOGLE_"


class AzureSecrets(BaseSettings):
    CLIENT_ID: str
    CLIENT_SECRET: str

    class Config:
        env_file = ".env"
        env_file_encoding = "utf-8"
        case_sensitive = False
        env_prefix = "AZURE_"


class UsersSettings(BaseSettings):
    TWIRP_SERVER_ADDRESS: str
    TWIRP_SERVER_PORT: int

    def get_full_address(self):
        return f"{self.TWIRP_SERVER_ADDRESS}:{self.TWIRP_SERVER_PORT}"

    class Config:
        env_file = ".env"
        env_file_encoding = "utf-8"
        case_sensitive = False
        env_prefix = "USERS_"


class Settings(BaseSettings):
    GOOGLE: GoogleSecrets = GoogleSecrets()
    MICROSOFT: AzureSecrets = AzureSecrets()
    USERS: UsersSettings = UsersSettings()
    TOKEN_COOKIE_NAME: str = "ProjectCalendarToken"
    DEFAULT_REDIRECT_URI: AnyHttpUrl = "http://localhost:8000"
    LOG_LEVEL: int | str = logging.DEBUG

    class Config:
        env_file = ".env"
        env_file_encoding = "utf-8"
        case_sensitive = False


config = Settings()
