from collections import namedtuple
from enum import Enum
from typing import Literal

from authenticator.config import config

_ProviderType = namedtuple(
    "_ProviderType", ("name", "consumer_key", "consumer_secret", "scope")
)


class Provider(Enum):
    GOOGLE = _ProviderType(
        "google",
        config.GOOGLE.CLIENT_ID,
        config.GOOGLE.CLIENT_SECRET,
        ["openid", "profile", "email"],
    )
    MICROSOFT = _ProviderType(
        "microsoft",
        config.MICROSOFT.CLIENT_ID,
        config.MICROSOFT.CLIENT_SECRET,
        ["openid", "profile", "email"],
    )
    INHOUSE = _ProviderType("self", "test", "test", ["openid", "profile", "email"])

    @classmethod
    def get_by_name(cls, name: str):
        for provider in cls:
            if provider.name == name:
                return provider


NamedProviders = Enum(
    "NamedProviders", {provider.name: provider.value.name for provider in Provider}
)
