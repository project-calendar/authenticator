import logging

from fastapi.staticfiles import StaticFiles
from starlette.middleware.authentication import AuthenticationMiddleware
from starlette.middleware.sessions import SessionMiddleware

from authenticator.config import config
from authenticator.middleware import TokenUserBackend, on_auth_error
from authenticator.routes import bootstrap_routers
from authenticator.utils import TypedApp
from users_twirp.service_twirp import ProjectCalendarUsersClient


def initialize_application():
    app = TypedApp()
    app.config = config
    app.twirp_client = ProjectCalendarUsersClient(
        address=config.USERS.get_full_address(), timeout=60
    )
    bootstrap_routers(app)
    app.logger = logging.Logger("project_calendar_users")
    app.logger.setLevel(config.LOG_LEVEL)
    app.add_middleware(SessionMiddleware, secret_key="secret")
    app.add_middleware(
        AuthenticationMiddleware,
        backend=TokenUserBackend(config.TOKEN_COOKIE_NAME),
        on_error=on_auth_error,
    )
    app.mount("/static", StaticFiles(directory="static"), name="static")
    return app
