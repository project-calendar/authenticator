from authlib.jose import jwt
from authlib.jose.errors import BadSignatureError, DecodeError
from starlette import status
from starlette.authentication import (
    AuthCredentials,
    AuthenticationBackend,
    AuthenticationError,
    BaseUser,
)
from starlette.requests import HTTPConnection
from starlette.responses import JSONResponse

from authenticator.routes.ui import not_authorized, router
from authenticator.utils import TypedRequest

UI_ROUTER_PATHS = [route.path for route in router.routes]


def on_auth_error(request: TypedRequest, exc: Exception):
    if request.url.path in UI_ROUTER_PATHS:
        request.scope["router"] = request.app
        return not_authorized(request)
    else:
        return JSONResponse(
            {"error": str(exc)}, status_code=status.HTTP_401_UNAUTHORIZED
        )


class TokenUser(BaseUser):
    def __init__(
        self,
        email: str | None = None,
        username: str | None = None,
        profile_picture_url: str | None = None,
    ) -> None:
        self.__anonymous = any(claim is None for claim in (email, username))
        self.email = email
        self.username = username
        self.profile_picture_url = profile_picture_url

    @classmethod
    def from_token(cls, token: str) -> "TokenUser":
        user = cls()
        data = jwt.decode(token, "test")
        try:
            user.email: str = data["sub"]
            user.username: str = data["username"]
            user.profile_picture_url: str | None = data.get("profile_picture_url")
            user.__anonymous = False
            return user
        except KeyError:
            return cls()

    @property
    def display_name(self) -> str:
        return self.email if not self.__anonymous else "Anonymous"

    @property
    def is_authenticated(self) -> bool:
        return self.__anonymous

    @property
    def scopes(self):
        return [] if self.__anonymous else ["authenticated"]

    def authentication_result(self) -> tuple[AuthCredentials, "TokenUser"]:
        return AuthCredentials(self.scopes), self


class TokenUserBackend(AuthenticationBackend):
    def __init__(self, cookie_name: str) -> None:
        self.__cookie_name = cookie_name
        super().__init__()

    async def authenticate(
        self, conn: HTTPConnection
    ) -> tuple[AuthCredentials, BaseUser]:
        token = conn.cookies.get(self.__cookie_name)
        try:
            user = TokenUser.from_token(token) if token else TokenUser()
        except DecodeError:
            raise AuthenticationError("Invalid token.")
        except BadSignatureError:
            raise AuthenticationError("Bad token signature.")
        return user.authentication_result()
