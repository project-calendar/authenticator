FROM python:3.10
WORKDIR /app

SHELL [ "/bin/bash", "-o", "pipefail", "-c" ]

ENV PYTHON_UNBUFFERED=1
ENV PATH=${PATH}:/root/.local/bin


RUN curl -sSL https://install.python-poetry.org | python3 -

COPY ./poetry.lock ./pyproject.toml ./

RUN poetry config virtualenvs.create false && poetry install --no-root --no-ansi --no-interaction

COPY README.md ./

COPY ./authenticator ./authenticator

COPY ./templates ./templates

COPY ./static ./static

COPY ./tests ./tests

RUN poetry install --no-interaction --no-ansi

COPY ./users_twirp ./users_twirp

COPY main.py .

ENTRYPOINT [ "hypercorn", "--bind", "0.0.0.0:8000", "main:app" ]