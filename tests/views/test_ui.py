from hashlib import md5
from unittest.mock import MagicMock
from uuid import uuid4

import pytest
from authlib.jose import jwt
from bs4 import BeautifulSoup
from fastapi import status
from fastapi.testclient import TestClient
from requests import Response
from twirp.errors import Errors
from twirp.exceptions import TwirpServerException

from authenticator.routes.flow import router as flow_router
from authenticator.routes.ui import router
from tests.utils import FactoryType
from users_twirp.service_pb2 import User


@pytest.fixture
def registration_user_factory():
    def _factory(d: dict[str, str] | None = None):
        user = {
            "email": "tester@organization.com",
            "username": "tester",
            "password": "test",
            "repeated_password": "test",
        }
        if d:
            user.update(d)
        return user

    return _factory


def basic_template_assertion(
    response: Response,
    status_code: int = status.HTTP_200_OK,
    template_name: str = "index.jinja2",
):
    assert response.status_code == status_code
    assert response.template.name == template_name
    assert "request" in response.context


def test_render_homepage(client: TestClient):
    response = client.get(router.url_path_for("homepage"))
    basic_template_assertion(response)
    soup = BeautifulSoup(response.text, features="html.parser")
    form = soup.find("form")
    assert form
    assert flow_router.url_path_for("login") in form.attrs["action"]
    assert all(
        flow_router.url_path_for("idp_login") in button.parent.attrs["href"]
        for button in form.find_all("button")
    ), "No buttons for logging in with external Identity providers were found."


def test_render_homepage__logged(
    client: TestClient, token: str, user_data: dict[str, str]
):
    client.cookies.set(client.app.config.TOKEN_COOKIE_NAME, token)
    response = client.get(router.url_path_for("homepage"))
    basic_template_assertion(response)
    assert f"Hi there, {user_data['username']}" in response.text
    assert user_data["sub"] in response.text
    client.cookies.clear()


def test_render_homepage__misformatted_token(client: TestClient):
    client.cookies.set(client.app.config.TOKEN_COOKIE_NAME, "test")
    response = client.get(router.url_path_for("homepage"))
    basic_template_assertion(
        response,
        template_name="not-authorized.jinja2",
        status_code=status.HTTP_401_UNAUTHORIZED,
    )
    assert response.cookies.get(client.app.config.TOKEN_COOKIE_NAME) is None
    assert all(context not in ("error", "user") for context in response.context)
    client.cookies.clear()


def test_render_homepage_bad_signature_token(client: TestClient):
    token = jwt.encode(
        {"alg": "HS256"},
        {
            "iss": "http://localhost",
            "sub": "test@test.com",
            "username": "test",
            "profile_picture_url": "http://localhost/test",
        },
        "bad_signature",
    ).decode("utf-8")
    client.cookies.set(client.app.config.TOKEN_COOKIE_NAME, token)
    response = client.get(router.url_path_for("homepage"))
    basic_template_assertion(
        response,
        template_name="not-authorized.jinja2",
        status_code=status.HTTP_401_UNAUTHORIZED,
    )
    assert response.cookies.get(client.app.config.TOKEN_COOKIE_NAME) is None
    client.cookies.clear()


def test_singup_page(client: TestClient):
    response = client.get(router.url_path_for("signup"))
    basic_template_assertion(response)
    assert response.context["ctx"] == "signup"
    assert response.context["user"] is None

    soup = BeautifulSoup(response.text, features="html.parser")
    form = soup.find_all("form")
    assert len(form) == 1


def test_signup_create_user__diffrent_passwords(
    client: TestClient, mocked_twirp_client: MagicMock
):
    response = client.post(
        router.url_path_for("create_user"),
        data={
            "email": "tester@organization.com",
            "username": "tester",
            "password": "test",
            "repeated_password": "passphrase",
        },
    )
    basic_template_assertion(response, status_code=status.HTTP_422_UNPROCESSABLE_ENTITY)
    assert response.context["error"]
    assert response.context["error"] == "Passwords are not the same."
    assert "Passwords are not the same." in response.text

    assert mocked_twirp_client.createUser.call_count == 0


def test_singup_create_user__service_unavailable(
    client: TestClient,
    mocked_twirp_client: MagicMock,
    registration_user_factory: FactoryType[dict[str, str]],
):
    response = client.post(
        router.url_path_for("create_user"), data=registration_user_factory()
    )
    basic_template_assertion(response, status_code=status.HTTP_503_SERVICE_UNAVAILABLE)
    assert response.context["error"]
    assert response.context["error"] == "Unknown exception."
    assert "Unknown exception." in response.text

    assert mocked_twirp_client.createUser.call_count == 1


def test_signup_create_user__bad_email(
    client: TestClient,
    mocked_twirp_client: MagicMock,
    registration_user_factory: FactoryType[dict[str, str]],
):
    response = client.post(
        router.url_path_for("create_user"),
        data=registration_user_factory({"email": "wrong@email"}),
    )
    assert response.status_code == status.HTTP_422_UNPROCESSABLE_ENTITY
    # basic_template_assertion(response, status_code=status.HTTP_422_UNPROCESSABLE_ENTITY)
    assert mocked_twirp_client.createUser.call_count == 0


def test_signup_create_user__bad_request(
    client: TestClient,
    mocked_twirp_client: MagicMock,
    registration_user_factory: FactoryType[dict[str, str]],
):
    mocked_twirp_client.createUser = MagicMock(
        side_effect=TwirpServerException(
            code=Errors.InvalidArgument, message="field message"
        )
    )
    response = client.post(
        router.url_path_for("create_user"),
        data=registration_user_factory(),
    )
    basic_template_assertion(response, status_code=status.HTTP_400_BAD_REQUEST)
    assert response.context["error"] == "message"
    assert "message" in response.text

    assert mocked_twirp_client.createUser.call_count == 1


def test_signup_create_user__already_exists(
    client: TestClient,
    mocked_twirp_client: MagicMock,
    registration_user_factory: FactoryType[dict[str, str]],
):
    mocked_twirp_client.createUser = MagicMock(
        side_effect=TwirpServerException(
            code=Errors.AlreadyExists, message="Already exists."
        )
    )
    response = client.post(
        router.url_path_for("create_user"), data=registration_user_factory()
    )
    basic_template_assertion(response, status_code=status.HTTP_405_METHOD_NOT_ALLOWED)
    assert response.context["error"] == "Already exists."
    assert "Already exists." in response.text

    assert mocked_twirp_client.createUser.call_count == 1


def test_signup_create_user__success(
    client: TestClient,
    mocked_twirp_client: MagicMock,
    registration_user_factory: FactoryType[dict[str, str]],
):
    user_data = registration_user_factory()
    mocked_twirp_client.createUser = MagicMock(
        return_value=User(
            id=str(uuid4()),
            email=user_data["email"],
            username=user_data["username"],
            profile_picture_url=f"https://gravatar.com/avatar/{md5(user_data['email'].encode('utf-8')).hexdigest()}",
        )
    )
    response = client.post(router.url_path_for("create_user"), data=user_data)
    basic_template_assertion(response, status_code=status.HTTP_201_CREATED)
    assert response.context.get("error") is None
    assert response.context["ctx"] == "signup-success"
    assert response.context["data"]["email"] == user_data["email"]
    assert user_data["email"] in response.text
