from unittest.mock import MagicMock

import pytest
from authlib.jose import jwt
from fastapi.testclient import TestClient

from authenticator import initialize_application
from authenticator.utils import TypedApp
from users_twirp.service_twirp import ProjectCalendarUsersClient


@pytest.fixture
def user_data(faker):
    return {
        "iss": "http://localhost:8000",
        "sub": faker.ascii_free_email(),
        "username": faker.name(),
        "profile_picture_url": faker.url(),
    }


@pytest.fixture
def token(user_data):
    yield jwt.encode(
        {"alg": "HS256"},
        user_data,
        "test",
    ).decode("utf-8")


@pytest.fixture(scope="session")
def app():
    app = initialize_application()
    yield app


@pytest.fixture(scope="session")
def client(app: TypedApp):
    yield TestClient(app)


@pytest.fixture()
def mocked_twirp_client(client: TestClient):
    client.app.twirp_client = MagicMock(
        wraps=ProjectCalendarUsersClient(str(client.base_url), 10)
    )
    yield client.app.twirp_client
