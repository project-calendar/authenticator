import pytest
from twirp.errors import Errors

from authenticator.utils import LoginErrors


@pytest.mark.parametrize("error", list(LoginErrors) + [None])
def test_loging_error__get_by_code(error: LoginErrors | None):
    assert LoginErrors.get_by_code(error.code if error else None) == error


@pytest.mark.parametrize("error_code", (Errors.PermissionDenied, Errors.NotFound))
def test_loging_error__get_by_error_code__bad_credentials(error_code: Errors):
    assert LoginErrors.get_by_error_code(error_code) == LoginErrors.BAD_CREDENTIALS


def test_login_error__get_by_error_code__idp_only():
    assert (
        LoginErrors.get_by_error_code(Errors.ResourceExhausted) == LoginErrors.IDP_ONLY
    )


@pytest.mark.parametrize(
    "error_code",
    list(
        filter(
            lambda err: err
            not in (Errors.PermissionDenied, Errors.NotFound, Errors.ResourceExhausted),
            Errors,
        )
    ),
)
def test_login_error__get_by_error_code__unknown(error_code: Errors):
    assert LoginErrors.get_by_error_code(error_code) == LoginErrors.UNKNOWN
