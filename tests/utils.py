from typing import Any, Generic, TypeVar

RT = TypeVar("RT")


class FactoryType(Generic[RT]):
    def __call__(self, *args: Any, **kwds: Any) -> RT:
        ...
